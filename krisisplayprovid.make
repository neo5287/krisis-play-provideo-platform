; ----------------
; KRISIS Play ProVideo Platform Makefile 
; Permanent URL: TBC
; Build Version: v0.001
; Build Name: build_0_001
; Build Description: Initial Repack of the demo install
; Filename: krisisplayprovid.make
; ----------------  

  
; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
  
core = 7.x
api = 2
      
; Modules
; --------

projects[captcha][subdir] = "contrib"
projects[captcha]][version] = "1.0"
projects[captcha]][type] = "module"

projects[ckeditor][subdir] = "contrib"
projects[ckeditor]][version] = "1.13"
projects[ckeditor]][type] = "module"

projects[ctools][subdir] = "contrib"
projects[ctools]][version] = "1.3"
projects[ctools]][type] = "module"

projects[date][subdir] = "contrib"
projects[date]][version] = "2.6"
projects[date]][type] = "module"

projects[field_permissions][subdir] = "contrib"
projects[field_permissions]][version] = "1.0-beta2"
projects[field_permissions]][type] = "module"

projects[flowplayer][subdir] = "contrib"
projects[flowplayer]][version] = "1.0-alpha1"
projects[flowplayer]][type] = "module"

projects[gravatar][subdir] = "contrib"
projects[gravatar]][version] = "1.x-dev"
projects[gravatar]][type] = "module"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.1"
projects[libraries][type] = "module"

projects[lightbox2][subdir] = "contrib"
projects[lightbox2][version] = "1.0-beta1"
projects[lightbox2][type] = "module"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.2"
projects[pathauto][type] = "module"

projects[provideo_helper][subdir] = "contrib"
projects[provideo_helper][type] = "module"
projects[provideo_helper][download][type] = "git"
projects[provideo_helper][download][url] = "https://bitbucket.org/cpmadmin/krisis-play-provideo-provideohelper.git"
projects[provideo_helper][download][branch] = "build_0_001"

projects[rate][subdir] = "contrib"
projects[rate][version] = "1.6"
projects[rate][type] = "module"

projects[sharethis][subdir] = "contrib"
projects[sharethis][version] = "2.5"
projects[sharethis][type] = "module"

projects[token][subdir] = "contrib"
projects[token][version] = "1.5"
projects[token][type] = "module"

projects[video][subdir] = "contrib"
projects[video][version] = "2.10"
projects[video][type] = "module"

projects[video_embed_field][subdir] = "contrib"
projects[video_embed_field][version] = "2.0-beta5"
projects[video_embed_field][type] = "module"

projects[videoimport][subdir] = "contrib"
projects[videoimport][type] = "module"
projects[videoimport][download][type] = "git"
projects[videoimport][download][url] = "https://bitbucket.org/cpmadmin/krisis-play-provideo-videoimport.git"
projects[videoimport][download][branch] = "build_0_001"

projects[views][subdir] = "contrib"
projects[views][version] = "3.7"
projects[views][type] = "module"

projects[votingapi][subdir] = "contrib"
projects[votingapi][version] = "2.11"
projects[votingapi][type] = "module"

; Libraries
; ---------

; ckeditor
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"
libraries[ckeditor][destination] = "libraries"
libraries[ckeditor][download][type] = "git"
libraries[ckeditor][download][url] = "https://bitbucket.org/cpmadmin/krisis-play-provideo-ckeditor.git"
libraries[ckeditor][download][branch] = "build_0_001"

; jwplayer
libraries[jwplayer][directory_name] = "jwplayer"
libraries[jwplayer][type] = "library"
libraries[jwplayer][destination] = "libraries"
libraries[jwplayer][download][type] = "git"
libraries[jwplayer][download][url] = "https://bitbucket.org/cpmadmin/krisis-play-provideo-jwplayer.git"
libraries[jwplayer][download][branch] = "build_0_001"
